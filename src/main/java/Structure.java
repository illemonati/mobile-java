public class Structure {
    double weight = 0;
    boolean balanced = true;

    public double getWeight() {
        return this.weight;
    }
    public boolean isBalanced() {
        return this.balanced;
    }
}

